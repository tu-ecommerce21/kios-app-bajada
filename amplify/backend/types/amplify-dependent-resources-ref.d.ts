export type AmplifyDependentResourcesAttributes = {
    "api": {
        "kiosappbajada": {
            "GraphQLAPIKeyOutput": "string",
            "GraphQLAPIIdOutput": "string",
            "GraphQLAPIEndpointOutput": "string"
        }
    }
}